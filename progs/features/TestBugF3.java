class TestBugF3 {
    public static void main(String[] args) {
        System.out.println(new Test().runBugF3());
    }
}

class Test {
    public int runBugF3() {
        boolean x = false;
        boolean y = true;
        int result = 0;
      
        // Testing bug: Returning false when x is false and y is true
        if (x || y) {
            result = result + 1;
        } else {
            result = result + 0;
        }
        
        return result;
    }
}

class TestBugF2 {
    public static void main(String[] args) {
        System.out.println(new Test().runBugF2());
    }

}

class Test {
    public int runBugF2() {
        boolean x = true;
        boolean y = false;
        int result = 0;
    
        // Testing bug: Evaluating y even when x is true
        if (x || y) {
            result = result + 1;
        } else {
            result = result - 1;
        }
        
        return result;
    }
}

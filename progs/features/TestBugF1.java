class TestBugF1 {
    public static void main(String[] args) {
        System.out.println(new Test().runBugF1());
    }
}

class Test {
    public int runBugF1() {
        int x = 3;
        int y = 3;
        int result = 0;
        
        // Testing bug: Allowing ORing of integers
        if (x || y) {
            result = result + 1;
        } else {
            result = result - 1;
        }
        
        return result;
    }
}